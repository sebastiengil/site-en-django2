from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Projet(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    imageDeCouv = models.ImageField(upload_to='images/', null=True)
    images = models.ImageField(upload_to='images/', null=True)

    def __str__(self):
        return self.name



