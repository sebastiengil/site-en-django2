from django.shortcuts import render
from .models import Conseils

def allconseils(request):
    conseils = Conseils.objects.all()
    return render(request, 'allconseils.html', {'conseils':conseils})

