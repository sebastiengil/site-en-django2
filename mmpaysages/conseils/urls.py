from django.urls import path
from . import views

urlpatterns = [
    path('conseils/', views.allconseils, name='allconseils')
]