from django.apps import AppConfig


class ConseilsConfig(AppConfig):
    name = 'conseils'
