from django.db import models

class Conseils(models.Model):
    conseil = models.TextField()
    mois = models.CharField(max_length=20)
    imageDeCouverture = models.ImageField(upload_to='images/', null=True)

    def __str__(self):
        return self.conseil