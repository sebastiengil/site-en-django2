from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse, HttpResponseRedirect
from django.core.mail import send_mail

from .forms import ContactForm

def contact_us(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            sender_name = form.cleaned_data['nom']
            sender_email = form.cleaned_data['email']

            message = "{0} has sent you a new message:\n\n{1}".format(sender_name, form.cleaned_data['message'])
            send_mail('Gil sebastien', message, sender_email, ['sebastien.gil@yahoo.com'])
            # return HttpResponse("Merci de m'avoir contacter")
            return HttpResponseRedirect("/contact/")

    else:
        form = ContactForm()

    return render(request, 'contact.html', {'form': form})

